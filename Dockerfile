FROM gitlab/gitlab-runner:alpine3.15-bleeding
WORKDIR /app
RUN apk add --no-cache \
        python3 \
        py3-pip \
	    jq \
        bash \
        git \
        curl \
        openssl \
        yarn \
        npm \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir \
        awscli \
    && rm -rf /var/cache/apk/*

RUN npm install typescript -g
RUN npm install serverless -g
RUN npm install serverless-offline -g
RUN npm install serverless-bundle -g
RUN npm install serverless-dynamodb-local -g
RUN npm install serverless-plugin-log-retention -g
